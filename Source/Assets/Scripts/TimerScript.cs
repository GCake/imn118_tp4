﻿using SDD.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerScript : MonoBehaviour
{

    [SerializeField]
    public float timeLeft = 90; //= 180;
    [HideInInspector]
    public int minute;
    [HideInInspector]
    public int seconde;

    private float timeLeftBegin = 90;

    private bool gameBegin = false;

    // Start is called before the first frame update
    void Start()
    {
        EventManager.Instance.AddListener<GamePlayEvent>(GameBeginFct);
        EventManager.Instance.AddListener<GameOverEvent>(GameOverFct);
        EventManager.Instance.AddListener<GameVictoryEvent>(GameVictoryFct);
    }

    
    private void OnDisable()
    {
        EventManager.Instance.RemoveListener<GamePlayEvent>(GameBeginFct);
        EventManager.Instance.RemoveListener<GameOverEvent>(GameOverFct);
        EventManager.Instance.RemoveListener<GameVictoryEvent>(GameVictoryFct);
    }


    // Update is called once per frame
    void Update()
    {
        if (gameBegin)
        {
            timeLeft -= Time.deltaTime;

            minute = (int)timeLeft / 60;
            seconde = (int)timeLeft % 60;
            EventManager.Instance.Raise(new TimeChangedEvent() { eMinute = minute, eSeconde = seconde, eTime = timeLeft });
        }
    }

    private void GameBeginFct(GamePlayEvent e)
    {
        gameBegin = true;
        timeLeft = timeLeftBegin;
    }
    private void GameOverFct(GameOverEvent e)
    {
        gameBegin = false;
        timeLeft = timeLeftBegin;
    }

    private void GameVictoryFct(GameVictoryEvent e)
    {
        gameBegin = false;
        timeLeft = timeLeftBegin;
    }

}
