﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

#region GameManager Events
public class GameMenuEvent : SDD.Events.Event
{
}
public class GamePlayEvent : SDD.Events.Event
{
}
public class GamePauseEvent : SDD.Events.Event
{
}
public class GameResumeEvent : SDD.Events.Event
{
}
public class GameOverEvent : SDD.Events.Event
{
}
public class GameVictoryEvent : SDD.Events.Event
{

    public float eBestTime { get; set; }
    public float eCurrentTime { get; set; }
}

public class GoalReachedEvent : SDD.Events.Event
{

}

public class ReloadSceneButtonEvent : SDD.Events.Event
{
}

public class GameStatisticsChangedEvent : SDD.Events.Event
{
    public int eNbPresentInRoom { get; set; }
    
}

public class GameTimedStatChangedEvent : SDD.Events.Event
{
    public float eBestTime { get; set; }
}
#endregion

#region MenuManager Events
public class EscapeButtonClickedEvent : SDD.Events.Event
{
}
public class PlayButtonClickedEvent : SDD.Events.Event
{
}
public class ResumeButtonClickedEvent : SDD.Events.Event
{
}
public class MainMenuButtonClickedEvent : SDD.Events.Event
{
}
#endregion

#region Enemy Event
public class EnemyHasBeenDestroyedEvent : SDD.Events.Event
{

}

public class PlayerHasBeenDetectedEvent : SDD.Events.Event
{
    public GameObject eLutin;
    public GameObject ePlayer;
}

#endregion

#region Level Events
//public class EnemiesHaveBeenRegisteredEvent : SDD.Events.Event
//{
//	public int eCount;
//}
public class AllPresentsHaveBeenDestroyedEvent : SDD.Events.Event
{

    
}

public class ChangeRoomEvent: SDD.Events.Event
{

}

public class TimeChangedEvent : SDD.Events.Event
{
    public int eMinute { get; set; }
    public int eSeconde { get; set; }

    public float eTime { get; set; }
}

public class EndGameUnLockedEvent : SDD.Events.Event
{

}


#endregion

#region Player
public class PlayerHasBeenHit : SDD.Events.Event
    {
        
    }
#endregion

#region Present

    public class PresentHasBeenDestroyEvent : SDD.Events.Event
    {
        
    }

#endregion