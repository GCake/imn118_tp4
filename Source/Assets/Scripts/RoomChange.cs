﻿using SDD.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomChange : MonoBehaviour
{
    [SerializeField]
    public Vector2 cameraChange;
    [SerializeField]
    public Vector3 newPositionPlayer;

    private CameraScript cam;
    private bool roomChangePossible = false;


    void Awake()
    {
        EventManager.Instance.AddListener<AllPresentsHaveBeenDestroyedEvent>(AllPresentsHaveBeenDestroyedFct);
    }

    private void OnDisable()
    {
        EventManager.Instance.RemoveListener<AllPresentsHaveBeenDestroyedEvent>(AllPresentsHaveBeenDestroyedFct);
    }

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main.GetComponent<CameraScript>();   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && roomChangePossible)
        {
            cam.min += cameraChange;
            cam.max += cameraChange;
            other.transform.position += newPositionPlayer;
            EventManager.Instance.Raise(new ChangeRoomEvent());
            roomChangePossible = false;
        }
    }

    private void AllPresentsHaveBeenDestroyedFct(AllPresentsHaveBeenDestroyedEvent e)
    {
        roomChangePossible = true;
    }
}
