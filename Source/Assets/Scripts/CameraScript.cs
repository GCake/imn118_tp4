﻿using SDD.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField]
    public Transform character;
    [SerializeField]
    public float smoothing;
    [HideInInspector]
    public Vector2 min;
    [HideInInspector]
    public Vector2 max;

    [SerializeField]
    public List<float[]> maxList;
    [SerializeField]
    public List<float[]> minList;

    int currentRoom;

    void Start()
    {
        maxList = new List<float[]>();
        minList = new List<float[]>();

        float[] maxRoom1 = new float[2];
        float[] maxRoom2 = new float[2];

        float[] minRoom1 = new float[2];
        float[] minRoom2 = new float[2];

        minRoom1[0] = -1.63f; //X 
        minRoom1[1] = 1.25f; //Y

        minRoom2[0] = -1.72f; //X 
        minRoom2[1] = 17.99f; //Y

        maxRoom1[0] = 3.69f; //X
        maxRoom1[1] = 7.83f; //Y

        maxRoom2[0] = 3.83f; //X
        maxRoom2[1] = 20.05f; //Y

        maxList.Add(maxRoom1);
        maxList.Add(maxRoom2);

        minList.Add(minRoom1);
        minList.Add(minRoom2);
    }

    private void Awake()
    {
        EventManager.Instance.AddListener<GamePlayEvent>(GameBeginFct);
        EventManager.Instance.AddListener<ChangeRoomEvent>(ChangeRoomFct);
    }

    private void OnDisable()
    {
        EventManager.Instance.RemoveListener<GamePlayEvent>(GameBeginFct);
        EventManager.Instance.AddListener<ChangeRoomEvent>(ChangeRoomFct);
    }

    private void GameBeginFct(GamePlayEvent e)
    {
        currentRoom = 0;
        max = new Vector2(maxList[currentRoom][0], maxList[currentRoom][1]);
        min = new Vector2(minList[currentRoom][0], minList[currentRoom][1]);
    }

    private void ChangeRoomFct(ChangeRoomEvent e)
    {
        currentRoom++;
        if (currentRoom >= maxList.Count)
            currentRoom = maxList.Count - 1;

        max = new Vector2(maxList[currentRoom][0], maxList[currentRoom][1]);
        min = new Vector2(minList[currentRoom][0], minList[currentRoom][1]);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(transform.position != character.position)
        {
            Vector3 targetPosition = new Vector3(character.position.x, character.position.y, transform.position.z);

            //Clamp : Permet de limiter la valeur
            targetPosition.x = Mathf.Clamp(targetPosition.x, min.x, max.x);
            targetPosition.y = Mathf.Clamp(targetPosition.y, min.y, max.y);

            transform.position = Vector3.Lerp(transform.position, targetPosition, smoothing);
        }
    }
}
