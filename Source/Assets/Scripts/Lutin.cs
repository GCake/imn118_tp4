﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

public class Lutin : SimpleGameStateObserver
{
    private Transform m_Transform;
    private Rigidbody2D m_Rigidbody;

    private enum DIRECTION { Top, Bottom, Left, Right };
    private int m_DirectionCount;
    private DIRECTION m_Direction;
    private int m_DirectionTimer;

    private bool m_WallHit;

    [Header("Lutin Properties")]
    [SerializeField]
    private float m_Speed = 1.0f;
    private float m_SpeedFactor = 1.0f;

    [Header("Detection Properties")]
    [SerializeField]
    private GameObject m_ExclamationPoint;
    [SerializeField]
    private GameObject m_DetectionPivot;
    private enum LUTIN_STATE { Normal, Searching, Chasing };
    private LUTIN_STATE m_State;
    private GameObject m_DetectedPlayer;
    private int m_ChasingTime = 5;
    private int m_SearchingTime = 5;

    private static System.Random rand;

    protected override void Awake()
    {
        base.Awake();
        m_Transform = GetComponent<Transform>();
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_DirectionCount= System.Enum.GetNames(typeof(DIRECTION)).Length;
        m_WallHit = false;
        if(rand == null)
        {
            rand = new System.Random();
        }
    }

    private void Start()
    {
        m_ExclamationPoint.SetActive(false);
        m_Direction = (DIRECTION)rand.Next(m_DirectionCount);
        m_State = LUTIN_STATE.Normal;
        RenewDirectionTimer();
        StartCoroutine("ChangeDirectionOnTime");
    }

    public override void SubscribeEvents()
    {
        base.SubscribeEvents();
        EventManager.Instance.AddListener<PlayerHasBeenDetectedEvent>(ChasePlayer);
    }

    public override void UnsubscribeEvents()
    {
        base.UnsubscribeEvents();
        EventManager.Instance.RemoveListener<PlayerHasBeenDetectedEvent>(ChasePlayer);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       
        if (m_State == LUTIN_STATE.Chasing)
        {
            if (!m_WallHit)
            {
                Chase();
            }
            else //Searching
            {
                m_Direction = ChooseDirection();
                RenewDirectionTimer();
                m_WallHit = false;
                m_State = LUTIN_STATE.Searching;
                m_SpeedFactor = 2.0f;
                StartCoroutine("SearchingTime");
            }
        }
        else
        {
            if (!m_WallHit)
            {
                Move(false);
                RotateDetection();
            }
            else
            {
                m_Direction = ChooseDirection();
                RenewDirectionTimer();
                m_WallHit = false;
            }
        }
    }

    private void Move(bool reverse)
    {
        switch (m_Direction)
        {
            case DIRECTION.Top:
                m_Rigidbody.MovePosition(new Vector2(m_Rigidbody.position.x, m_Rigidbody.position.y + m_Speed * m_SpeedFactor * Time.fixedDeltaTime * (reverse ? -1 : 1)));
                break;
            case DIRECTION.Bottom:
                m_Rigidbody.MovePosition(new Vector2(m_Rigidbody.position.x, m_Rigidbody.position.y - m_Speed * m_SpeedFactor * Time.fixedDeltaTime * (reverse ? -1 : 1)));
                break;
            case DIRECTION.Left:
                m_Rigidbody.MovePosition(new Vector2(m_Rigidbody.position.x - m_Speed * m_SpeedFactor * Time.fixedDeltaTime * (reverse ? -1 : 1), m_Rigidbody.position.y));
                break;
            case DIRECTION.Right:
                m_Rigidbody.MovePosition(new Vector2(m_Rigidbody.position.x + m_Speed * m_SpeedFactor * Time.fixedDeltaTime * (reverse ? -1 : 1), m_Rigidbody.position.y));
                break;
            default:
                break;
        }
    }

    private void RotateDetection()
    {
        Quaternion from;
        Quaternion to;
        switch (m_Direction)
        {
            case DIRECTION.Top:
                from = m_DetectionPivot.transform.rotation;
                to = Quaternion.Euler(new Vector3(0, 0, 0));
                m_DetectionPivot.transform.rotation = Quaternion.RotateTowards(from, to, 10);
                break;
            case DIRECTION.Bottom:
                from = m_DetectionPivot.transform.rotation;
                to = Quaternion.Euler(new Vector3(0, 0, 180));
                m_DetectionPivot.transform.rotation = Quaternion.RotateTowards(from, to, 10);
                break;
            case DIRECTION.Left:
                from = m_DetectionPivot.transform.rotation;
                to = Quaternion.Euler(new Vector3(0, 0, 90));
                m_DetectionPivot.transform.rotation = Quaternion.RotateTowards(from, to, 10);
                break;
            case DIRECTION.Right:
                from = m_DetectionPivot.transform.rotation;
                to = Quaternion.Euler(new Vector3(0, 0, 270));
                m_DetectionPivot.transform.rotation = Quaternion.RotateTowards(from, to, 10);
                break;
            default:
                break;
        }
    }

    private DIRECTION ChooseDirection()
    {
        List<int> possibleDirection = new List<int>();
        for (int i = 0; i < m_DirectionCount; ++i)
        {
            if((int)m_Direction != i)
            {
                possibleDirection.Add(i);
            }
        }
        return (DIRECTION)possibleDirection[rand.Next(possibleDirection.Count)];
    }

    private void RenewDirectionTimer()
    {
        m_DirectionTimer = rand.Next(6, 16); //3 a 8sec
    }

    private IEnumerator ChangeDirectionOnTime()
    {
        while(m_DirectionTimer >= 0)
        {
            --m_DirectionTimer;
            yield return new WaitForSeconds(0.5f);
        }
        m_Direction = ChooseDirection();
        RenewDirectionTimer();
        StartCoroutine("ChangeDirectionOnTime");
        yield return null;
    }

    private void ChasePlayer(PlayerHasBeenDetectedEvent e)
    {
        if (e.eLutin.Equals(this.gameObject))
        {
            if(m_State != LUTIN_STATE.Chasing)
            {
                StartCoroutine("ChasingTime");
            }
            m_ChasingTime = 5;
            m_State = LUTIN_STATE.Chasing;
            m_SpeedFactor = 2.5f;
            m_DetectedPlayer = e.ePlayer;
            m_ExclamationPoint.SetActive(true);
        }
    }

    private void Chase()
    {
        Vector3 direction = (m_DetectedPlayer.transform.position - m_Transform.position).normalized;
        m_Rigidbody.MovePosition(new Vector2(
            m_Rigidbody.position.x + m_Speed * m_SpeedFactor * Time.fixedDeltaTime * direction.x, 
            m_Rigidbody.position.y + m_Speed * m_SpeedFactor * Time.fixedDeltaTime * direction.y));

        //rotation
        Quaternion from = m_DetectionPivot.transform.rotation;
        Quaternion to = Quaternion.Euler(Vector3.forward * Vector3.SignedAngle(Vector3.up, direction, Vector3.forward));
        m_DetectionPivot.transform.rotation = Quaternion.RotateTowards(from, to, 10);
    }

    private IEnumerator ChasingTime()
    {
        while(m_ChasingTime > 0)
        {
            --m_ChasingTime;
            if (m_State != LUTIN_STATE.Chasing) yield break;
            yield return new WaitForSeconds(1.0f);
        }
        m_State = LUTIN_STATE.Searching;
        m_SpeedFactor = 2.0f;
        m_ExclamationPoint.SetActive(true);
        StartCoroutine("SearchingTime");
    }

    private IEnumerator SearchingTime()
    {
        for(int i = 0; i < m_SearchingTime; ++i)
        {
            if (m_State != LUTIN_STATE.Searching) yield break;
            yield return new WaitForSeconds(1.0f);
        }
        m_State = LUTIN_STATE.Normal;
        m_SpeedFactor = 1.0f;
        m_ExclamationPoint.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!collision.CompareTag("Player") && !collision.CompareTag("LutinDetectionRadius"))
        {
            if (!m_WallHit)
            {
                m_WallHit = true;
                Move(true);
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player") && !collision.CompareTag("LutinDetectionRadius"))
        {
            if (!m_WallHit)
            {
                m_WallHit = true;
                Move(true);
            }
        }
    }
}
