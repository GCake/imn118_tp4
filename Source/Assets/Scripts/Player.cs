﻿using SDD.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{

    [HideInInspector]
    public bool touchedEnabled;

    void Awake()
    {
        EventManager.Instance.AddListener<GameVictoryEvent>(GameVictoryFct);
    }

    private void OnDisable()
    {
        EventManager.Instance.RemoveListener<GameVictoryEvent>(GameVictoryFct);
    }

    protected override void Start()
    {
        touchedEnabled = true;
        base.Start();
    }

    // Update is called once per frame
    protected override void Update()
    {
        GetInput();
        base.Update();
    }

    private void GetInput()
    {
        direction = Vector2.zero;
        if (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.W))
        {
            direction += Vector2.up;
        }
        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.A))
        {
            direction += Vector2.left;
        }
        if (Input.GetKey(KeyCode.S))
        {
            direction += Vector2.down;
        }
        if (Input.GetKey(KeyCode.D))
        {
            direction += Vector2.right;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
           attackRoutine = StartCoroutine(Attack());
        }
    }

    private IEnumerator Attack()
    {
        if (!isAttacking && !IsMoving)
        {
            isAttacking = true;

            MyAnimator.SetBool("attack", isAttacking);

            yield return new WaitForSeconds(1); // hardcoded cast time for debugging

            Debug.Log("attack done");
            StopAttack();
        }
    }

    private void GameVictoryFct(GameVictoryEvent e)
    {
        touchedEnabled = false;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
       
        if (collision.gameObject.CompareTag("EnemyCollision") && touchedEnabled)
        {
            EventManager.Instance.Raise(new PlayerHasBeenHit());
        }
    }
 
}
