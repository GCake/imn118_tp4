﻿using SDD.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameScript : MonoBehaviour
{
    [SerializeField]
    GameObject startObj;

    private bool endGameLock = true;

    void Start()
    {
        EventManager.Instance.AddListener<EndGameUnLockedEvent>(EndGameUnLockedFct);
    }

    private void OnDisable()
    {
        EventManager.Instance.RemoveListener<EndGameUnLockedEvent>(EndGameUnLockedFct);
    }


    private void EndGameUnLockedFct(EndGameUnLockedEvent e)
    {
        startObj.SetActive(true);
        endGameLock = false;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        
        if(collision.gameObject.CompareTag("Player") && !endGameLock)
        {
            EventManager.Instance.Raise(new GoalReachedEvent());
        }
    }
}
