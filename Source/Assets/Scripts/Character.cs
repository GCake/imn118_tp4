﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{


    [SerializeField]

    private float speed;

    protected Animator MyAnimator;

    private Rigidbody2D myRigidbody;

    protected bool isAttacking = false;

    protected Coroutine attackRoutine;

    protected Vector2 direction;



    // Start is called before the first frame update
    protected virtual void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        MyAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        HandleLayers();

    }

    private void FixedUpdate()
    {
        Move();
    }

    public bool IsMoving
    {
        get
        {
            return direction.x != 0 || direction.y != 0;
        }
    }
    public void Move()
    {
        myRigidbody.velocity = direction.normalized * speed;
    }

    public void ActivateLayer(string layerName)
        {
            for (int i=0; i < MyAnimator.layerCount; i++)
            {
                MyAnimator.SetLayerWeight(i, 0);
            }
            MyAnimator.SetLayerWeight(MyAnimator.GetLayerIndex(layerName), 1);
       
        }

    public void HandleLayers()
    
    {
        if (IsMoving)
        {
            ActivateLayer("WalkLayer");
            MyAnimator.SetFloat("x", direction.x);
            MyAnimator.SetFloat("y", direction.y);
            StopAttack();
        }

        else if (isAttacking){
            ActivateLayer("AttackLayer");
        }
    }


    public void StopAttack()
    {
        if (attackRoutine != null)
        {
            StopCoroutine(attackRoutine);
            isAttacking = false;
            MyAnimator.SetBool("attack", isAttacking);
        }
    }
}