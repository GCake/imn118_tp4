﻿using SDD.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PresentScript : MonoBehaviour
{
    [SerializeField]
    public int lifePresent;
    private float attack;

    private void Update()
    {
        attack = Input.GetAxis("Jump");
    }

   
    public void OnCollisionStay2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag("Player") && attack > 0)
        {
            if (lifePresent > 0)
                lifePresent--;

            if (lifePresent <= 0)
            {
                EventManager.Instance.Raise(new PresentHasBeenDestroyEvent());
                Destroy(this.gameObject);
            }
            
        }
    }
}
