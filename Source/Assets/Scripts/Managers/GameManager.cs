﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;
using UnityEngine.SceneManagement;

public enum GameState { gameMenu,gamePlay,gamePause,gameOver,gameVictory}

public class GameManager : Manager<GameManager> {

	//Game State
	private GameState m_GameState;
	public bool IsPlaying { get { return m_GameState == GameState.gamePlay; } }

	// TIME SCALE
	private float m_TimeScale;
	public float TimeScale { get { return m_TimeScale; } }
	void SetTimeScale(float newTimeScale)
	{
		m_TimeScale = newTimeScale;
		Time.timeScale = m_TimeScale;
	}

    //SCORE
    private float m_bestTime;
    private float m_currentTime;

    //PRESENT
    private int m_NbPresent;
    public int NBbPresent
    {
        get { return m_NbPresent; }
        set
        {
            m_NbPresent = value;
           
        }
    }

    public float BestTime
    {
        get
        {
            return PlayerPrefs.GetFloat("BEST_SCORE", 0);
        }
        set
        {
            PlayerPrefs.SetFloat("BEST_SCORE",  value);
        }
	}

    public float CurrentTime
    {
        get
        {

            return m_currentTime;
        }
        set
        {
            
            m_currentTime = value;

        }
    }

    //Lives
    [SerializeField] private int m_NStartLives;

	private int m_NLives;
	public int NLives { get { return m_NLives; } }

    public override void SubscribeEvents()
	{
		base.SubscribeEvents();
		EventManager.Instance.AddListener<MainMenuButtonClickedEvent>(MainMenuButtonClicked);
		EventManager.Instance.AddListener<PlayButtonClickedEvent>(PlayButtonClicked);
		EventManager.Instance.AddListener<ResumeButtonClickedEvent>(ResumeButtonClicked);
		EventManager.Instance.AddListener<EscapeButtonClickedEvent>(EscapeButtonClicked);

        EventManager.Instance.AddListener<PresentHasBeenDestroyEvent>(PresentHasBeenDestroyFct);
        EventManager.Instance.AddListener<ChangeRoomEvent>(ChangeRoomFct);


        EventManager.Instance.AddListener<PlayerHasBeenHit>(PlayerHasBeenHitFct);
        EventManager.Instance.AddListener<ReloadSceneButtonEvent>(ReloadSceneButton);
        EventManager.Instance.AddListener<TimeChangedEvent>(TimeChangeFct);
        EventManager.Instance.AddListener<GoalReachedEvent>(GoalReachedFct);
        
    }

 

    public override void UnsubscribeEvents()
	{
		base.UnsubscribeEvents();

		EventManager.Instance.RemoveListener<MainMenuButtonClickedEvent>(MainMenuButtonClicked);
		EventManager.Instance.RemoveListener<PlayButtonClickedEvent>(PlayButtonClicked);
		EventManager.Instance.RemoveListener<ResumeButtonClickedEvent>(ResumeButtonClicked);
		EventManager.Instance.RemoveListener<EscapeButtonClickedEvent>(EscapeButtonClicked);

        EventManager.Instance.RemoveListener<PresentHasBeenDestroyEvent>(PresentHasBeenDestroyFct);
        EventManager.Instance.RemoveListener<ChangeRoomEvent>(ChangeRoomFct);

        EventManager.Instance.RemoveListener<PlayerHasBeenHit>(PlayerHasBeenHitFct);
        EventManager.Instance.RemoveListener<ReloadSceneButtonEvent>(ReloadSceneButton);
        EventManager.Instance.RemoveListener<TimeChangedEvent>(TimeChangeFct);

        EventManager.Instance.AddListener<GoalReachedEvent>(GoalReachedFct);
    }


    private void ReloadSceneButton(ReloadSceneButtonEvent e)
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name); 
    }

    private void PlayerHasBeenHitFct(PlayerHasBeenHit e)
    {
       Over();     
    }

    protected override IEnumerator InitCoroutine()
	{
        while (!MenuManager.Instance.IsReady) yield return null;

        Menu();
		yield break;
	}

	private void InitNewGame()
	{
        m_NbPresentCurrentRoom = m_NbPresentPerRoom[0];

        if(BestTime > 75f)
            BestTime = 0.0f;
        EventManager.Instance.Raise(new GameStatisticsChangedEvent() { eNbPresentInRoom = m_NbPresentCurrentRoom });
        EventManager.Instance.Raise(new GameTimedStatChangedEvent() { eBestTime = BestTime });
    }

    private void ChangeRoomFct(ChangeRoomEvent e)
    {
        m_IndiceCurrentRoom++;
        if (m_IndiceCurrentRoom >= m_NbPresentPerRoom.Count)
            m_IndiceCurrentRoom = m_NbPresentPerRoom.Count;

        m_NbPresentCurrentRoom = m_NbPresentPerRoom[m_IndiceCurrentRoom];
        EventManager.Instance.Raise(new GameStatisticsChangedEvent() { eNbPresentInRoom = m_NbPresentCurrentRoom });
    }

    private void TimeChangeFct(TimeChangedEvent e)
    {
        CurrentTime = e.eTime;

        if(CurrentTime <= 0)
            EventManager.Instance.Raise(new PlayerHasBeenHit());
    }

    //Present Events

    [SerializeField]
    private List<int> m_NbPresentPerRoom;
    int m_IndiceCurrentRoom = 0;
    int m_NbPresentCurrentRoom = 0;

    private void PresentHasBeenDestroyFct(PresentHasBeenDestroyEvent e)
    {
        m_NbPresentCurrentRoom--;
        if (m_NbPresentCurrentRoom < 0)
            m_NbPresentCurrentRoom = 0;

        EventManager.Instance.Raise(new GameStatisticsChangedEvent() { eNbPresentInRoom = m_NbPresentCurrentRoom });

        if (m_NbPresentCurrentRoom == 0 && m_IndiceCurrentRoom == (m_NbPresentPerRoom.Count - 1))
            EventManager.Instance.Raise(new EndGameUnLockedEvent());
        else if (m_NbPresentCurrentRoom == 0)
            EventManager.Instance.Raise(new AllPresentsHaveBeenDestroyedEvent());
       
    }

 

	//Buttons Events
	private void MainMenuButtonClicked(MainMenuButtonClickedEvent e)
	{
		Menu();
	}

	private void PlayButtonClicked(PlayButtonClickedEvent e)
	{
        Play();
	}

	private void ResumeButtonClicked(ResumeButtonClickedEvent e)
	{
		Resume();
	}

	private void EscapeButtonClicked(EscapeButtonClickedEvent e)
	{
		if(IsPlaying)
			Pause();
	}

	//Game State events
	private void Menu()
	{
		SetTimeScale(0);
		m_GameState = GameState.gameMenu;
		EventManager.Instance.Raise(new GameMenuEvent());
	}

	private void Play()
	{
		InitNewGame();
		SetTimeScale(1);
		m_GameState = GameState.gamePlay;
		EventManager.Instance.Raise(new GamePlayEvent());
	}

	private void Pause()
	{
		SetTimeScale(0);
		m_GameState = GameState.gamePause;
		EventManager.Instance.Raise(new GamePauseEvent());
	}

	private void Resume()
	{
		SetTimeScale(1);
		m_GameState = GameState.gamePlay;
		EventManager.Instance.Raise(new GameResumeEvent());
	}

	private void Over()
	{
        if (m_GameState != GameState.gameVictory)
        {
            SetTimeScale(0);
            m_GameState = GameState.gameOver;
            EventManager.Instance.Raise(new GameOverEvent());
        }
	}

	private void Victory()
	{
		SetTimeScale(0);
		m_GameState = GameState.gameVictory;
        BestTime = Mathf.Max(BestTime, CurrentTime);
        EventManager.Instance.Raise(new GameVictoryEvent() { eBestTime = BestTime, eCurrentTime = CurrentTime});
	}


    private void GoalReachedFct(GoalReachedEvent e)
    {
        Victory();
    }

}
