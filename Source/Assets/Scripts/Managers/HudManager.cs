﻿using SDD.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HudManager : Manager<HudManager> {

	[Header("Texts")]
	[SerializeField] private Text m_TxtNbPresent;
	[SerializeField] private Text m_TxtTime;
    [SerializeField] private Text m_TxtBestScore;
    [SerializeField] private Text m_TxtBestScorePanelVictory;
    [SerializeField] private Text m_TxtCurrentScorePanelVictory;

    [SerializeField] GameObject m_HUDPanel;

    void Awake()
    {
        EventManager.Instance.AddListener<TimeChangedEvent>(TimeChangedFct);
        EventManager.Instance.AddListener<GameVictoryEvent>(GameVictoryFct);
        EventManager.Instance.AddListener<GameOverEvent>(GameOverFct);
        EventManager.Instance.AddListener<PlayButtonClickedEvent>(PlayButtonClickedFct);
        EventManager.Instance.AddListener<GameTimedStatChangedEvent>(GameTimedStatChangedFct);
    }

   

    private void OnDisable()
    {
        EventManager.Instance.RemoveListener< TimeChangedEvent> (TimeChangedFct);
        EventManager.Instance.RemoveListener<GameVictoryEvent>(GameVictoryFct);
        EventManager.Instance.RemoveListener<GameOverEvent>(GameOverFct);
        EventManager.Instance.RemoveListener<PlayButtonClickedEvent>(PlayButtonClickedFct);
        EventManager.Instance.RemoveListener<GameTimedStatChangedEvent>(GameTimedStatChangedFct);
    }

  

    protected override IEnumerator InitCoroutine()
	{
		yield break;
	}

	protected override void GameStatisticsChanged(GameStatisticsChangedEvent e)
	{
        if(e.eNbPresentInRoom !=-1)
            m_TxtNbPresent.text = e.eNbPresentInRoom.ToString();

    }

    private void GameTimedStatChangedFct(GameTimedStatChangedEvent e)
    {
        int minuteBest = (int)e.eBestTime / 60;
        int secondeBest = (int)e.eBestTime % 60;
        m_TxtBestScore.text = minuteBest.ToString() + " : " + secondeBest.ToString();
    }

    private void TimeChangedFct(TimeChangedEvent e)
    {
        m_TxtTime.text = e.eMinute.ToString() + " : " + e.eSeconde.ToString();
    }

    private void GameVictoryFct(GameVictoryEvent e)
    {
        int minuteBest = (int)e.eBestTime / 60;
        int secondeBest  = (int)e.eBestTime % 60;

        int minute = (int)e.eCurrentTime / 60;
        int seconde = (int)e.eCurrentTime % 60;

        m_TxtBestScore.text = minuteBest.ToString() + " : " + secondeBest.ToString();
        m_TxtBestScorePanelVictory.text = minuteBest.ToString() + " : " + secondeBest.ToString();
        m_TxtCurrentScorePanelVictory.text = minute.ToString() + " : " + seconde.ToString();

        m_HUDPanel.SetActive(false);
    }

    private void PlayButtonClickedFct(PlayButtonClickedEvent e)
    {
        m_HUDPanel.SetActive(true);
    }

    private void GameOverFct(GameOverEvent e)
    {
        m_HUDPanel.SetActive(false);
    }
}
