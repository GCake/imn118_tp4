﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

public class LutinDetectionRadius : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Lutin;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            EventManager.Instance.Raise(new PlayerHasBeenDetectedEvent() {
                eLutin = m_Lutin,
                ePlayer = collision.gameObject
            });
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            EventManager.Instance.Raise(new PlayerHasBeenDetectedEvent()
            {
                eLutin = m_Lutin,
                ePlayer = collision.gameObject
            });
        }
    }
}
