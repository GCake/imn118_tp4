Le répertoire Build\Windows contient un executable pour Windows.
Lancez New Unity Project.exe pour jouer. (Jouer en fenêtré pour pouvoir quitter avec la croix rouge)

Le répertoire Source contient les répertoires Assets et ProjectSettings du projet Unity.
Pour les utiliser, créer un nouveau projet Unity et remplacer les deux répertoires du projet par ceux-là.
La version Unity qui a été utilisé est la version 2019.2.3f1.